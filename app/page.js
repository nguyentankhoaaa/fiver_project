"use client"
import { useContext, useEffect, useState, } from "react"
import Context from "./store/context"
import Jobs from "./pages/jobs/page";
import { jobServ } from "./services/jobServ";
import { useForm } from "react-hook-form";


export default function Home() {
  const { userInfor } = useContext(Context)
  const [listJob, setListJob] = useState([])
  const { handleSubmit, register, getValues } = useForm();
  const [filterResult, setFilterResult] = useState([]);
  useEffect(() => {
    jobServ.getJob().then((res) => {
      setListJob(res.data.content)
    })
      .catch((err) => {
        console.log(err);
      });
  }, [])

  const onsubmit = (data) => {
    let value = getValues("search");
    if (value !== "") {
      let filterJob = listJob.filter((item) => {
        return (
          Object.values(item.tenCongViec).join('').toLowerCase().includes(value.toLowerCase()) ||
          Object.values(item.moTa).join('').toLowerCase().includes(value.toLowerCase())
        )
      })
      setFilterResult(filterJob)
    } else {
      setFilterResult(listJob)
    }

    console.log(filterResult);
  }
  return (
    <main className="home-fiver container mt-5">
      <div className="nav-bar">
        <div className="d-flex d-flex justify-content-between">
          {
            userInfor == null ? (
              <div></div>
            ) : (
              <h2 className="fw-semibold">Nice to see you, {userInfor?.user?.name}</h2>
            )
          }
          <form onSubmit={handleSubmit(onsubmit)} className=" mx-5 d-flex">
            <input className="form-control me-2"  {...register("search")} type="search" name="search" placeholder="Search" aria-label="Search" />
            <button className="btn btn-outline-success" type="submit">Search</button>
          </form>
        </div>
        <div className="my-4 d-flex  ">
          <div className="tailor-fiver  d-flex justify-content-between"
          >
            <div className="item-1" >
              <i className="fa fa-chess" style={{ fontSize: "36px" }} />

            </div>
            <div className="item-2 mx-4">
              <h5>Tailor Fiver for your needs</h5>
              <h6>Tell us about your business.</h6>
            </div>
            <div className="item-3" style={{ marginLeft: "90px" }}>
              <i className="fa fa-angle-right fs-4"></i>
            </div>
          </div>
          <div className="tailor-fiver mx-5 d-flex justify-content-between align-items-center"
          >
            <div className="item-1" >
              <i className="fa fa-magic" style={{ fontSize: "36px" }} />

            </div>
            <div className="item-2 mx-4">
              <h5>Create a brief to get proposals from sellers</h5>
              <h6>Let us do the searching.</h6>
            </div>
            <div className="item-3" style={{ marginLeft: "90px" }}>
              <i className="fa fa-angle-right fs-4"></i>
            </div>
          </div>
        </div>

      </div>
      <Jobs filterResult={filterResult} />
    </main>
  )
} 
