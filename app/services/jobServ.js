import { http } from "./config"

export const jobServ = {
    getMenuCategoryJob: () => {
        return http.get(`/api/cong-viec/lay-menu-loai-cong-viec`)
    },
    getJob: () => {
        return http.get("/api/cong-viec")
    },
    getJobId: (id) => {
        return http.get(`/api/cong-viec/${id}`)
    },
    postThueJob: (form) => {
        return http.post('/api/thue-cong-viec', form);
    },
    getThueJob: () => {
        return http.get("/api/thue-cong-viec")
    },
    getJobTheoChiTIet: (id) => {
        return http.get(`/api/cong-viec/lay-cong-viec-theo-chi-tiet-loai/${id}`)
    }

}
