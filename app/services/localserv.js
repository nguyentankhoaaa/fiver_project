export const localServ = {
    get: () => {
        let dataJSON = localStorage?.getItem('USER_INFO');

        return JSON?.parse(dataJSON);
    },
    set: (userInfo) => {
        let dataJSON = JSON?.stringify(userInfo);
        localStorage?.setItem('USER_INFO', dataJSON);
    },
    remove: () => {
        localStorage?.removeItem('USER_INFO');
    },
    getUser: () => {
        let dataJSON = localStorage?.getItem('USER');

        return JSON?.parse(dataJSON);
    },
    setUser: (userInfo) => {
        let dataJSON = JSON?.stringify(userInfo);
        localStorage?.setItem('USER', dataJSON);
    },
    removeUser: () => {
        localStorage?.removeItem('USER');
    },
}       