import axios from "axios";
import { localServ } from "./localserv";


const BASE_URL = "https://fiverrnew.cybersoft.edu.vn";
const tokenCybersoft = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA1NiIsIkhldEhhblN0cmluZyI6IjAzLzA0LzIwMjQiLCJIZXRIYW5UaW1lIjoiMTcxMjEwMjQwMDAwMCIsIm5iZiI6MTY4MzMwNjAwMCwiZXhwIjoxNzEyMjUwMDAwfQ.YeDhc_oSixV2XtFPDzcpxFhBos5832JpQpndHNoqZLk";
const configHeader = () => {
    return {
        tokenCybersoft: tokenCybersoft,
        token: localServ.get()?.token,
    }
}

export const http = axios.create({
    baseURL: BASE_URL,
    headers: configHeader(),
})


