import { http } from "./config"

export const userServ = {
    postSignUp: (signUpForm) => {
        return http.post("/api/auth/signup", signUpForm)
    },
    postLogin: (loginForm) => {
        return http.post("/api/auth/signin", loginForm)
    },
    getUser: () => {
        return http.get("/api/users")
    },
    uploadAvatar: (file) => {
        return http.post("/api/users/upload-avatar", file);
    },
    getUserById: (id, data) => {
        return http.get(`/api/users/${id}`, data)
    },
    putUserId: (id, data) => {
        return http.put(`/api/users/${id}`, data)
    }
}
