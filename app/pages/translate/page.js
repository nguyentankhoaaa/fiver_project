import React from 'react'
import { initI18n } from "i18n";
const Translate = () => {
    initI18n({
        locales: ["en", "vi"],
        defaultLocale: "en",
        resources: {
            en: {
                title: "English",
                hello: "Hello",
            },
            vi: {
                title: "Tiếng Việt",
                hello: "Xin chào",
            },
        },
    });

    const button = document.getElementById("change-language");
    const languageSelection = document.getElementById("language-selection");

    button.addEventListener("click", () => {
        // Lấy ngôn ngữ được chọn
        const locale = languageSelection.value;

        // Thay đổi ngôn ngữ
        initI18n.changeLocale(locale);

        // Thay đổi nội dung của nút
        button.textContent = locale === "en" ? "Tiếng Việt" : "English";
    });

    languageSelection.addEventListener("change", () => {
        // Thay đổi ngôn ngữ
        const locale = languageSelection.value;

        // Dịch nội dung của trang web
        initI18n.changeLocale(locale);

        // Thay đổi nội dung của nút
        button.textContent = locale === "en" ? "Tiếng Việt" : "English";
    });
    return (
        <div>button id="change-language"&gt;English
            <select id="language-selection">
                <option value="en">English</option>
                <option value="vi">Tiếng Việt</option>
            </select></div>

    )
}

export default Translate