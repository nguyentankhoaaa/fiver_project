"use client"

import React from 'react'
import { useEffect, useState } from "react"
import Card from 'react-bootstrap/Card';
import Slider from "react-slick";
import { jobServ } from '@/app/services/jobServ';
import Link from 'next/link';

const Jobs = ({ filterResult }) => {
    function SampleNextArrow(props) {
        const { className, style, onClick } = props;
        return (
            <div
                className={className}
                style={{ ...style, display: "block", backgroundColor: "black", borderRadius: "50%" }}
                onClick={onClick}
            />
        );
    }

    function SamplePrevArrow(props) {
        const { className, style, onClick } = props;
        return (
            <div
                className={className}
                style={{
                    ...style, display: "block",
                    backgroundColor: "black", borderRadius: "50%",
                }}
                onClick={onClick}
            />
        );
    }
    const [jobs, setJobs] = useState([]);
    const [users, setUsers] = useState([]);
    const settings = {
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        className: "center",
        infinite: true,
        centerPadding: "60px",
        slidesToShow: 5,
        swipeToSlide: true,
        autoplay: true,
    };

    useEffect(() => {
        jobServ.getJob().then((res) => {
            setJobs(res.data.content)
        })
            .catch((err) => {
                console.log(err);
            });
    }, [])

    return (
        <div>
            {
                filterResult.length === 0 ? <>
                    <div className="body-job my-4 row px-3 py-3">
                        <h5 className="mb-2 fw-bolder">Continue browsing <i className="fa fa-arrow-right "></i></h5>
                        <Slider {...settings} >
                            {jobs.slice(0, 6).map((item) => {
                                return <Card key={item.id}>
                                    <Link href={`/pages/jobs/${item.id}`}>
                                        <Card.Img variant="top" src={item.hinhAnh} />

                                    </Link>

                                    <Card.Body>
                                        <Card.Title className="fs-6" >{item.tenCongViec.substr(1, 40)}...</Card.Title>
                                        <Card.Text >
                                            <i className="fa fa-star" /> {item.saoCongViec}
                                            <span className="text-secondary fs-6 mx-1" >({item.danhGia})</span>
                                            <h6 className="my-2 fw-bolder">From US{item.giaTien}$</h6>
                                        </Card.Text>

                                    </Card.Body>
                                </Card>
                            })}
                        </Slider>
                    </div>
                    <div className="bodys-job my-4 row px-3 py-4">
                        <h5 className="mb-2 fw-bolder">Most popular Gigs in  <span style={{ color: "#446EE7" }}>Website Development</span></h5>
                        <Slider {...settings} >
                            {jobs.slice(7, 13).map((item) => {
                                return <Card key={item.id}>
                                    <Link href={`/pages/jobs/${item.id}`}>
                                        <Card.Img variant="top" src={item.hinhAnh} />

                                    </Link>
                                    <Card.Body>
                                        <Card.Title className="fs-6" >{item.tenCongViec.substr(1, 40)}...</Card.Title>
                                        <Card.Text >
                                            <i className="fa fa-star" /> {item.saoCongViec}
                                            <span className="text-secondary fs-6 mx-1" >({item.danhGia})</span>
                                            <h6 className="my-2 fw-bolder">From US{item.giaTien}$</h6>
                                        </Card.Text>

                                    </Card.Body>
                                </Card>
                            })}
                        </Slider>

                    </div>
                    <div className="bodys-job my-4 row px-3 py-4">
                        <h5 className="mb-2 fw-bolder">Gigs you may like</h5>
                        <Slider {...settings} >
                            {jobs.slice(14, 20).map((item) => {
                                return <Card key={item.id}>
                                    <Link href={`/pages/jobs/${item.id}`}>
                                        <Card.Img variant="top" src={item.hinhAnh} />

                                    </Link>
                                    <Card.Body>
                                        <Card.Title className="fs-6" >{item.tenCongViec.substr(1, 40)}...</Card.Title>
                                        <Card.Text >
                                            <i className="fa fa-star" /> {item.saoCongViec}
                                            <span className="text-secondary fs-6 mx-1" >({item.danhGia})</span>
                                            <h6 className="my-2 fw-bolder">From US{item.giaTien}$</h6>
                                        </Card.Text>

                                    </Card.Body>
                                </Card>
                            })}
                        </Slider>
                    </div>
                    <div className="bodys-job my-4 row px-3 py-4" style={{ backgroundColor: "#fafafa" }}>
                        <h5 className="mb-2 fw-bolder">Verified Pro services in  <a className="underline"> Website Development </a></h5>
                        <Slider {...settings} >
                            {jobs.slice(21, 27).map((item) => {
                                return <Card key={item.id}>
                                    <Link href={`/pages/jobs/${item.id}`}>
                                        <Card.Img variant="top" src={item.hinhAnh} />

                                    </Link>
                                    <Card.Body>
                                        <Card.Title className="fs-6" >{item.tenCongViec.substr(1, 40)}...</Card.Title>
                                        <Card.Text >
                                            <i className="fa fa-star" /> {item.saoCongViec}
                                            <span className="text-secondary fs-6 mx-1" >({item.danhGia})</span>
                                            <h6 className="my-2 fw-bolder">From US{item.giaTien}$</h6>
                                        </Card.Text>

                                    </Card.Body>
                                </Card>
                            })}
                        </Slider>



                    </div>
                </> : <>
                    <div className="body-job my-4 row px-3 py-3">
                        <h5 className="mb-2 fw-bolder">Continue browsing <i className="fa fa-arrow-right "></i></h5>
                        <Slider {...settings} >
                            {filterResult.slice(0, 6).map((item) => {
                                return <Card key={item.id}>
                                    <Link href={`/pages/jobs/${item.id}`}>
                                        <Card.Img variant="top" src={item.hinhAnh} />

                                    </Link>

                                    <Card.Body>
                                        <Card.Title className="fs-6" >{item.tenCongViec.substr(1, 40)}...</Card.Title>
                                        <Card.Text >
                                            <i className="fa fa-star" /> {item.saoCongViec}
                                            <span className="text-secondary fs-6 mx-1" >({item.danhGia})</span>
                                            <h6 className="my-2 fw-bolder">From US{item.giaTien}$</h6>
                                        </Card.Text>

                                    </Card.Body>
                                </Card>
                            })}
                        </Slider>
                    </div>
                </>
            }
        </div>
    )
}

export default Jobs