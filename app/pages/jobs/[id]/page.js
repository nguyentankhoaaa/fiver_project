
"use client"
import { jobServ } from '@/app/services/jobServ'
import { localServ } from '@/app/services/localserv';
import { userServ } from '@/app/services/userServ';
import Context from '@/app/store/context';
import { message } from 'antd';
import React, { useContext, useEffect, useState } from 'react';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';

const DetailJob = ({ params }) => {
    const [job, setJob] = useState({});
    const [user, setUser] = useState([]);
    const [thueJob, setThueJob] = useState([])
    const { userInfor } = useContext(Context)
    useEffect(() => {
        jobServ.getJobId(params.id).then((res) => {
            setJob(res.data.content)
        })
            .catch((err) => {
                console.log(err);
            });
        userServ.getUser().then((res) => {
            setUser(res.data.content)
        })
            .catch((err) => {
                console.log(err);
            });
        jobServ.getThueJob().then((res) => {
            setThueJob(res.data.content)
        })
            .catch((err) => {
                console.log(err);
            });

    }, [])

    let newJob = {
        maCongViec: job?.id,
        maNguoiThue: userInfor?.user?.id,
        ngayThue: new Date().toLocaleDateString(),

    }

    const handleThueJob = () => {

        if (thueJob?.length > 0) {
            if (userInfor?.token) {
                let checkIndex = thueJob?.findIndex(item => item?.maCongViec === job?.id && item?.maNguoiThue === userInfor?.user?.id)
                if (checkIndex == -1) {
                    jobServ.postThueJob(newJob).then((res) => {
                        setThueJob(res.data.content)
                        message.success("Ordered success!")

                    })
                        .catch((err) => {
                            console.log(err);
                        });
                } else {
                    message.warning("You have ordered !")
                }
            } else {
                message.warning("Please sign in account !")
            }
        } else {
            message.warning("You have ordered !")
        }
    }

    const checkUser = () => {
        return user?.filter(user => user.id == job?.nguoiTao)
    }
    let adMin = checkUser()[0];

    return (
        <div className='detail-job  my-5 row ' style={{ margin: "0 90px" }}>
            <div className='item-content  col-6'>
                <h3 className='fw-bolder'>{job?.tenCongViec}</h3>
                <div className='title row '>
                    <div className='item-1 col-2 '>
                        <img src={adMin?.avatar} alt="" style={{ height: "70px", width: "70px", borderRadius: "50%" }} />
                    </div>
                    <div className='item-2 col-6'>
                        <h5>{adMin?.name} <span className='text-secondary fs-6'> {adMin?.email}</span></h5>
                        <p>  <i className="fa fa-star" /> {job.saoCongViec}
                            <span className="text-secondary fs-6 mx-1" >({job?.danhGia})</span></p>

                        <p>{job?.moTaNgan?.substr(1, 70)}...</p>
                    </div>

                    <img src={job.hinhAnh} className="w-75" alt="" />
                    <h5 className='fw-bolder mt-3'>About this gig</h5>
                    <h6 className='text-secondary'>WANNA Build Your Own Website? Develop a STUNNING WordPress Website.</h6>
                    <p>My Expertise in modern theme design and development
                        I will develop and design themes from FIGMA, XD, SKETCH, PSD t
                        o fully functional WordPress Responsive web. I will develop E-Commerce,
                        Booking, Real Estate, Sell Landing Page, Creative Business, Informative
                        and many more types of websites
                        with Elementor or Elementor Pro and I also can do custom work with Elementor and ACF.</p>

                    <ul>
                        <h6 className='fw-bolder'>Feartures</h6>
                        <li>Creative elementor web</li>
                        <li>Elementor landing page</li>
                        <li>Custom theme with elementor</li>
                        <li>FIgma to elementor</li>
                        <li>PSD to elementor</li>
                        <li>SKETCH to elementor</li>
                        <li>XD to elementor</li>
                        <li>Clone website using elementor pro</li>
                        <li>Responsive and professional design</li>
                    </ul>
                    <ol>
                        <h6 className='fw-bolder'>Why me</h6>
                        <li>Fast Responses </li>
                        <li>100% Professional Work</li>
                        <li>Quick Delivery</li>

                    </ol>
                </div>
            </div>
            <div className='item-pay col-4 '
            >
                <div className='item-sticky'>
                    <Tabs
                        defaultActiveKey="profile"
                        id="fill-tab-example  position-fixed-top'"
                        className="mb-3   "
                        fill
                    >

                        <Tab eventKey="home" title="Basic" className='container tab' >
                            <div className='d-flex justify-content-between py-2'>
                                <p className='fw-bolder'>Custom LandingPage</p>
                                <h5>US${job.giaTien}</h5>
                            </div>
                            <p className='text-secondary fw-bolder my-3'>1 Page Custom Wix Design | Mobile Responsive+ Basic SEO</p>
                            <p className='my-4'><i className="fa fa-clock" /> 7 Days Delivery
                                <span className='mx-4'><i className="fa fa-redo"></i> 5 Revisions</span>
                            </p>
                            <div className='solution'>
                                <p><i className="fa fa-check"></i> 3 figures</p>
                                <p><i className="fa fa-check"></i> Include source file</p>
                                <p><i className="fa fa-check"></i> Printable resolution file</p>
                                <p><i className="fa fa-check"></i> Add background/scene</p>
                                <p><i className="fa fa-check"></i> Include colors in illustration</p>
                                <p><i className="fa fa-check"></i> Include entire body illustration</p>
                            </div>
                            <button className='fw-bolder w-100' onClick={() => { handleThueJob() }}
                                style={{
                                    borderRadius: "10px",
                                    backgroundColor: "black", padding: "5px", color: "white"
                                }}
                            >Continue</button>
                            <h6 className='mt-4'>Save up to 20% with <span className='fw-bolder'
                                style={{ color: "#026a5d", cursor: "pointer" }}>Subscribe to Save</span>
                            </h6>

                        </Tab>
                        <Tab eventKey="profile" title="Standard">
                            <div className='d-flex justify-content-between py-2'>
                                <p className='fw-bolder'>Premium Website (Most Popular)</p>
                                <h5>US${job.giaTien * 2}</h5>
                            </div>
                            <p className='text-secondary fw-bolder my-3'>1 Page Custom Wix Design | Mobile Responsive+ Basic SEO</p>
                            <p className='my-4'><i className="fa fa-clock" /> 14 Days Delivery
                                <span className='mx-4'><i className="fa fa-redo"></i> Ullmlted Revisions</span>
                            </p>
                            <div className='solution'>
                                <p><i className="fa fa-check"></i> 3 figures</p>
                                <p><i className="fa fa-check"></i> Include source file</p>
                                <p><i className="fa fa-check"></i> Printable resolution file</p>
                                <p><i className="fa fa-check"></i> Add background/scene</p>
                                <p><i className="fa fa-check"></i> Include colors in illustration</p>
                                <p><i className="fa fa-check"></i> Include entire body illustration</p>
                            </div>
                            <button className='fw-bolder w-100' onClick={() => { handleThueJob() }}
                                style={{
                                    borderRadius: "10px",
                                    backgroundColor: "black", padding: "5px", color: "white"
                                }}
                            >Continue</button>
                            <h6 className='mt-4'>Save up to 20% with <span className='fw-bolder'
                                style={{ color: "#026a5d", cursor: "pointer" }}>Subscribe to Save</span>
                            </h6>
                        </Tab>
                        <Tab eventKey="longer-tab" title="Premium">
                            <div className='d-flex justify-content-between py-2'>
                                <p className='fw-bolder'>VIP Professional Website</p>
                                <h5>US${job.giaTien * 3}</h5>
                            </div>
                            <p className='text-secondary fw-bolder my-3'>Custom Wix Design/ Re-design up to 10 Pages | Mobile Responsive | Basic SEO</p>
                            <p className='my-4'><i className="fa fa-clock" /> 21 Days Delivery
                                <span className='mx-4'><i className="fa fa-redo"></i> Ullmlted Revisions</span>
                            </p>
                            <div className='solution'>
                                <p><i className="fa fa-check"></i> 3 figures</p>
                                <p><i className="fa fa-check"></i> Include source file</p>
                                <p><i className="fa fa-check"></i> Printable resolution file</p>
                                <p><i className="fa fa-check"></i> Add background/scene</p>
                                <p><i className="fa fa-check"></i> Include colors in illustration</p>
                                <p><i className="fa fa-check"></i> Include entire body illustration</p>
                            </div>
                            <button className='fw-bolder w-100' onClick={() => { handleThueJob() }}
                                style={{
                                    borderRadius: "10px",
                                    backgroundColor: "black", padding: "5px", color: "white"
                                }}
                            >Continue</button>
                            <h6 className='mt-4'>Save up to 20% with <span className='fw-bolder'
                                style={{ color: "#026a5d", cursor: "pointer" }}>Subscribe to Save</span>
                            </h6>
                        </Tab>


                    </Tabs>
                </div>
            </div>
        </div>
    )
}

export default DetailJob