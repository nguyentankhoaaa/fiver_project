"use client"
import Button from 'react-bootstrap/Button';
import Context from '@/app/store/context';
import React, { useContext, useEffect, useState } from 'react'
import { set, useForm } from 'react-hook-form';
import ModalShow from '@/app/components/modal/page';
import { userServ } from '@/app/services/userServ';
import FormData from 'form-data';
import { message } from 'antd';
import { localServ } from '@/app/services/localserv';


const DetailProfile = () => {
    const { register, } = useForm();
    const [modalShow, setModalShow] = useState(false);
    const { userInfor, user, setUser } = useContext(Context);
    const handleFileChange = (event) => {
        let formData = new FormData();
        const file = event.target.files[0];
        formData.append("formFile", file);
        userServ?.uploadAvatar(formData).then((res) => {
            localServ.setUser(res.data.content)
            setUser(res.data.content)
            message.success("UpLoad Image success !")
        })
            .catch((err) => {
                message.warning("Please Try agian !")
                console.log(err);
            });
    };
    return (
        <div className='detail-profile bg-light'>
            <div className='profile d-flex container justify-content-between ' >
                <div className='item-1'>
                    <div className='image-upload position-relative '
                        style={{ borderTop: " 1px solid #dadbdd" }}>
                        <form id='img-special' className='form-image position-relative '
                            style={user ? {
                                backgroundImage: `url(${user?.avatar})`,
                                backgroundSize: "cover", backgroundRepeat: "no-repeat",
                                backgroundPosition: "center",
                            } :
                                {
                                    backgroundImage:
                                        `url("https://tse4.mm.bing.net/th?id=OIP.f8UHknNKYlD1OuYWu5dNmAAAAA&pid=Api&P=0&h=180")`,
                                    backgroundSize: "contain", backgroundRepeat: "no-repeat",
                                    backgroundPosition: "center"
                                }}
                            onChange={handleFileChange}>

                            <input className='upload'
                                {...register("file")} type="file" name="file"
                                accept="image/jpeg,image/png,image/gif" />
                            <div className='status position-absolute'>
                                <p className='dot d-inline-block mb-0 '>.</p>
                                <p className='online d-inline-block mb-0'>Online</p>
                            </div>
                        </form>
                        <div className='my-1 text-center'>
                            <h6 className='fw-semibold position-absolute' style={{ top: "80px", left: "170px", color: "#7a7d85" }}>@{userInfor?.user?.name}</h6>
                        </div>
                        <div className='detail-infor  d-flex justify-content-between my-4'
                            style={{ borderTop: " 1px solid #dadbdd" }}>
                            <div className='my-4 '>
                                <p style={{ color: "#62646A" }}>
                                    <i className="fa fa-map-marker-alt fs-6 " style={{ marginRight: "20px" }}></i>
                                    From</p>
                                <p style={{ color: "#62646A" }}>
                                    <i className="fa fa-user fs-6 " style={{ marginRight: "20px" }}></i>
                                    Member since</p>
                            </div>
                            <div className='my-4 mx-5'>
                                <p className='fw-semibold' style={{ color: "#62646A" }}>
                                    VietNam</p>
                                <p className='fw-semibold' style={{ color: "#62646A" }}>
                                    {userInfor?.user?.birthday}</p>
                            </div>
                        </div>
                    </div>
                    <div className='infor-upload'>
                        <div className='title d-flex justify-content-between'>
                            <h6 className='text-dark fw-bolder'> Descripton</h6>
                            <Button className='btn btn-light' onClick={() => setModalShow(true)}>
                                <p className='mb-0'><i className="fa fa-pen-alt"></i></p>
                            </Button>
                            <ModalShow show={modalShow}
                                onHide={() => setModalShow(false)} />
                        </div>
                        <div className='content d-flex justify-content-between '
                            style={{ borderBottom: " 1px solid #dadbdd" }}>
                            <div className='item-1 my-4' >
                                <p style={{ color: "#62646A" }}>Name:</p>
                                <p style={{ color: "#62646A" }}>Phone:</p>
                                <p style={{ color: "#62646A" }}>Birthday:</p>
                            </div>
                            <div className='item-2 my-4 mx-3' >
                                <p style={{ color: "#62646A" }}>{user?.name}</p>
                                <p style={{ color: "#62646A" }}>{user?.phone}</p>
                                <p style={{ color: "#62646A" }}>{user?.birthday}</p>
                            </div>


                        </div>
                        <div className='title mt-4'>
                            <h6 className='text-dark fw-bolder'> Languages</h6>
                        </div>
                        <div className='content  '
                            style={{ borderBottom: " 1px solid #dadbdd" }}>
                            <div className='item-1 my-4' >
                                <p style={{ color: "#62646A" }}>English - <span style={{ color: "#b2b2b2" }}>Basic</span></p>
                                <p style={{ color: "#62646A" }}>Vietnamese (Tiếng Việt) - <span style={{ color: "#b2b2b2" }}>Native/Bilingual</span></p>
                            </div>
                        </div>
                        <div className='title d-flex justify-content-between my-4'
                            style={{ borderBottom: " 1px solid #dadbdd" }}>
                            <h6 className='text-dark fw-bolder'> Skills</h6>
                            <Button className='btn btn-light' onClick={() => setModalShow(true)}>
                                <p className='mb-0'><i className="fa fa-pen-alt"></i></p>
                            </Button>
                            <ModalShow show={modalShow}
                                onHide={() => setModalShow(false)} />
                        </div>
                        <div className='title d-flex justify-content-between my-4 pb-3'

                        >
                            <h6 className='text-dark fw-bolder'> Certification</h6>
                            <Button className='btn btn-light' onClick={() => setModalShow(true)}>
                                <p className='mb-0'><i className="fa fa-pen-alt"></i></p>
                            </Button>
                            <ModalShow show={modalShow}
                                onHide={() => setModalShow(false)} />
                        </div>
                    </div>
                    <div className='share-infor py-3 px-4'>
                        <p className='fw-bolder'>Shared activity information</p>
                        <p style={{ color: "#62646A" }}>In order to provide the best possible work and service, s
                            ome information about your activity on Fiverr may be shared with sellers.
                            <span style={{ cursor: "pointer", color: "#00698c" }}>Mannage Settings</span>
                        </p>
                    </div>
                </div>
                <div className='item-2 w-100 '
                    style={{ marginTop: "50px", marginLeft: "100px" }}>
                    <div className='share-infor text-center'>
                        <img src="https://tse2.mm.bing.net/th?id=OIP.fTRjiDpnXpLNSli9z7EmqwHaHa&pid=Api&P=0&h=180"

                            alt="" />
                        <h5 className='my-4 fw-semibold' style={{ color: "#404145" }}>Ready to earn on your own terms?</h5>
                        <button className='btn btn-success'>Become a seller</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default DetailProfile