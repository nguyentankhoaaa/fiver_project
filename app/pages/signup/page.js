"use client"

import { userServ } from '@/app/services/userServ';
import { message } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import React from 'react'
import { useForm } from 'react-hook-form'


const SignUpPage = () => {
    const { register, handleSubmit, watch, formState: { errors } } = useForm();
    const router = useRouter()
    const onsubmit = (data) => {
        const { ...newData } = data;
        userServ.postSignUp(newData).then((res) => {
            message.success("Sign Up success !")
            console.log(res.data.content);
            router.push("/pages/login")
        }).catch((err) => {
            message.error("Email exist !")
        })

    }
    return (
        <div className='page-signup'>
            <div className='container signup '>
                <form onSubmit={handleSubmit(onsubmit)} className='form'>
                    <h2 className='text-center title'>REGISTER</h2>
                    <form className="d-flex flex-row align-items-center">
                        <label htmlFor="name"><i className="fa fa-user" /></label>
                        <input className='input' type="text" name="name"
                            id="name" placeholder='your username'
                            {...register("name", { required: true })}
                        />
                    </form>
                    {errors.name?.type === "required" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}>Username is required</p>}
                    <form className="d-flex flex-row align-items-center">
                        <label htmlFor="email"><i className="fa fa-envelope" />
                        </label>
                        <input className='input' type="email" name="eamil"
                            id="email" placeholder='your email'
                            {...register("email", { required: true, pattern: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/ })} />
                    </form>
                    {errors.email?.type === "required" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}>Email is required</p>}
                    {errors.email?.type === "pattern" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}>Invalid Email Address</p>}

                    <form className="d-flex flex-row align-items-center">
                        <label htmlFor="password"><i className="fa fa-lock" />
                        </label>
                        <input className='input' type="password" name="password"
                            id="password" placeholder='your password'
                            {...register("password", { required: true, minLength: 6 })}
                        />
                    </form>
                    {errors.password?.type === "required" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}>Password is required</p>}
                    {errors.password?.type === "minLength" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}> Password must be 6 character</p>}
                    <form className="d-flex flex-row align-items-center">
                        <label htmlFor="repassword"><i className="fa fa-key" />
                        </label>
                        <input className='input' type="password" name="repassword"
                            id="repassword" placeholder='repeat password'
                            {...register("repassword", {
                                required: true, validate: (val) => {
                                    if (watch('password') != val) {
                                        return false
                                    }
                                }
                            })}
                        />
                    </form>
                    {errors.repassword?.type === "required" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}> Password is required</p>}
                    {errors.repassword?.type === "validate" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}> Password not same</p>}
                    <form className="d-flex flex-row align-items-center">
                        <label htmlFor="phone"><i className="fa fa-phone" />
                        </label>
                        <input className='input' type="phone" name="phone"
                            id="phone" placeholder='your phone'
                            {...register("phone", { required: true, pattern: /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/ })}
                        />
                    </form>
                    {errors.phone?.type === "required" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}> Phone-number is required</p>}
                    {errors.phone?.type === "pattern" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}>Invalid Phone-number</p>}
                    <form className="d-flex flex-row align-items-center ">
                        <label htmlFor="birthday"><i className="fa fa-birthday-cake" />
                        </label>
                        <input className='input' type="date" name="birthday"
                            id="birthday" placeholder='your birthday'
                            {...register("birthday", { required: true })} />
                    </form>
                    {errors.birthday?.type === "required" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}> Birth-day is required</p>}
                    <form   >
                        <label htmlFor="gender" ><i className="fa fa-transgender-alt"></i></label>
                        <input className='radio' type="radio" id='male' value={true}
                            {...register("gender", { register: true })}
                            name='gender' />
                        <label htmlFor="male" className='radio-label'>Male</label>
                        <input className='radio' type="radio" id='female' value={false}
                            {...register("gender", { register: true })}
                            name='gender' />
                        <label htmlFor="male" className='radio-label'>Female</label>

                        {errors.gender?.type === "required" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}>Gender is required</p>}
                    </form>
                    <button type="submit" >Sign Up</button>
                </form >
                <div className='item-image'>
                    <img src="https://demo5.cybersoft.edu.vn/static/media/signup.bd994738c4eb8deb2801.jpg"
                        alt="" style={{ width: "85%", height: "80%", marginLeft: "50px" }} />
                    <Link className='link' href='/pages/login' style={{ marginLeft: "130px" }}>I am already member</Link>
                </div>
            </div>
        </div>

    )
}

export default SignUpPage