"use client"
import { localServ } from '@/app/services/localserv';
import { userServ } from '@/app/services/userServ';

import Context from '@/app/store/context';
import { message } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import React, { useContext } from 'react'
import { useForm } from 'react-hook-form';

const LoginPage = () => {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const { setUserInfor, setUser } = useContext(Context)
    const router = useRouter();
    const onsubmit = (data) => {
        userServ.postLogin(data).then((res) => {
            message.success("Login success!");
            localServ.set(res.data.content);
            setUserInfor(res.data.content)
            localServ.setUser(res.data.content.user)
            setUser(res.data.content.user)
            router.push("/")
            console.log(res.data.content);
        })
            .catch((err) => {
                message.error("Login error")
            });
    };

    return (
        <div className='page-login'>
            <div className='login container'>
                <div className='item-image'>
                    <img src="https://demo5.cybersoft.edu.vn/static/media/signin.6f1c72291c1ec0817ded.jpg"
                        alt="" style={{ width: "85%", height: "80%", marginLeft: "50px" }} />
                </div>
                <form onSubmit={handleSubmit(onsubmit)} >
                    <h2 className='text-center title'>Sign In to Fiver</h2>
                    <form className='form d-flex flex-row align-items-center' action="">
                        <label htmlFor="email"><i className="fa fa-envelope" />
                        </label>
                        <input className='input' type="email"
                            name='email' id='email' placeholder='Your Email'
                            {...register("email", { required: true, pattern: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/ })} />

                    </form>
                    {errors.email?.type === "required" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}>Email is required</p>}
                    {errors.email?.type === "pattern" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}>Invalid Email Address</p>}
                    <form className='form d-flex flex-row align-items-center' action="">
                        <label htmlFor="password"><i className="fa fa-key" />

                        </label>
                        <input className='input' type="password"
                            name='password' id='password' placeholder='Your Password'
                            {...register("password", { required: true, minLength: 6 })}
                        />

                    </form>
                    {errors.password?.type === "required" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}>Password is required</p>}
                    {errors.password?.type === "minLength" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}> Password must be 6 character</p>}
                    <div className='nav-end d-flex justify-content-between align-items-center'>
                        <button type='submit'>Login</button>
                        <Link className='link' href="/pages/signup">Register now ?</Link>
                    </div>
                </form>
            </div>

        </div>
    )
}

export default LoginPage