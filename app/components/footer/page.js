import React from 'react'

const Footer = () => {
    return (
        <div className=' footer '
            style={{ bottom: "0", left: "0", right: "0" }}>
            <div className='container'>
                <div className='rows '>
                    <div className='cols'>
                        <p className='nav-link active '>Categories</p>
                        <p className='nav-link'>Graphics & Design</p>
                        <p className='nav-link'>Digital Marketing</p>
                        <p className='nav-link'>Writing & Translation</p>
                        <p className='nav-link'>Video & Animation</p>
                        <p className='nav-link'>Music & Audio</p>
                        <p className='nav-link'>Programming & Tech</p>
                        <p className='nav-link'>Data</p>
                        <p className='nav-link'>Business</p>
                        <p className='nav-link'>Lifestyle</p>
                        <p className='nav-link'>Sitemap</p>

                    </div>
                    <div className='cols'>
                        <p className='nav-link active '>About</p>
                        <p className='nav-link'>Careers</p>
                        <p className='nav-link'>Press & News</p>
                        <p className='nav-link'>Partnerships</p>
                        <p className='nav-link'>Privacy Policy</p>
                        <p className='nav-link'>Terms of Service</p>
                        <p className='nav-link'>Intellectual Property Claims</p>
                        <p className='nav-link'>Investor Relations</p>
                    </div>
                    <div className='cols'>
                        <p className='nav-link active '>Support</p>
                        <p className='nav-link'>Help & Support</p>
                        <p className='nav-link'>Trust & Safety</p>
                        <p className='nav-link'>Selling on Fiverr</p>
                        <p className='nav-link'>Buying on Fiverr</p>
                    </div>
                    <div className='cols'>
                        <p className='nav-link active '>Comunity</p>
                        <p className='nav-link'>Events</p>
                        <p className='nav-link'>Blog</p>
                        <p className='nav-link'>Forum</p>
                        <p className='nav-link'>Community Standards</p>
                        <p className='nav-link'>Podcast</p>
                        <p className='nav-link'>Affiliates</p>
                        <p className='nav-link'>Invite a Friend</p>
                        <p className='nav-link'>Become a Seller</p>
                    </div>
                    <div className='cols'>
                        <p className='nav-link active '>More From Fiver</p>
                        <p className='nav-link'>Fiverr Business</p>
                        <p className='nav-link'>Fiverr Pro</p>
                        <p className='nav-link'>Fiverr Studios</p>
                        <p className='nav-link'>Fiverr Logo Maker</p>
                        <p className='nav-link'>Fiverr Guides</p>
                        <p className='nav-link'>Get Inspired</p>
                        <p className='nav-link'>Fiverr Select</p>
                        <p className='nav-link'>ClearVoice
                            <span className='nav-span'>Content Marketing</span></p>
                        <p className='nav-link'>Fiverr Workspace
                            <span className='nav-span'>Invoice Software</span></p>
                        <p className='nav-link'>Learn
                            <span className='nav-span'>Online Courses</span></p>
                        <p className='nav-link'>Working Not Working</p>
                    </div>
                </div>
                <div className='footer-end' style={{ display: "flex" }} >
                    <div className='item-1 ' style={{ display: "flex" }}>


                        <a className="navbar-brand font-monospace fw-semibold fs-2 text text-center position-relative" href="/">Fiver
                            <i className="fab fa-r-project text-secondary  position-absolute "
                                style={{ verticalAlign: "middle", marginLeft: "5px" }} ></i>
                        </a>



                        <div>     <p>© Fiverr International Ltd. 2022</p></div>

                    </div>
                    <div className='item-2' style={{ display: "flex" }}>
                        <a href><i className="fab fa-twitter" /></a>
                        <a href><i className="fab fa-facebook" /></a>
                        <a href><i className="fab fa-instagram" /></a>
                        <a href><i className="fab fa-pinterest" /></a>
                        <a href><i className="fab fa-linkedin" /></a>
                        <a className="nav-link" href="#"><i className="fa fa-globe" style={{ verticalAlign: "middle", marginRight: "3px", marginBottom: "2px" }} />
                            English
                        </a>

                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer