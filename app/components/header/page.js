'use client'
import Dropdown from 'react-bootstrap/Dropdown';
import Link from "next/link"
import { useContext, } from "react"
import Context from '@/app/store/context';
import { localServ } from '@/app/services/localserv';
import { message } from 'antd';
import Menu from './menu/page';


const Header = () => {

    const { userInfor, user } = useContext(Context);
    const handleLogOut = () => {
        location.href = "/";
        localServ.remove();
        localServ.removeUser();
        message.success("Log Out success !")
    }
    return (
        <div className="header">
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container">
                    <Link className="navbar-brand font-monospace fw-bold    fs-2 text text-center " onClick={() => {
                        location.href = "/"
                    }} href="/">Fiver
                        <i className="fab fa-cloudscale  text-success" style={{ verticalAlign: "middle", marginLeft: "5px" }} ></i></Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon" />
                    </button>
                    <div className="collapse navbar-collapse " style={{ display: 'flex ', justifyContent: "space-between" }} id="navbarSupportedContent">
                        <div>

                        </div>
                        <div>
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex justify-content-center align-items-center">
                                <li className="nav-item">
                                    <a className="nav-link active" aria-current="page" href="#">Fiver Business</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Explore</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#"><i className="fa fa-globe" style={{ verticalAlign: "middle", marginRight: "3px", marginBottom: "2px" }} />
                                        English
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">US$ USD</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Become a Seller</a>
                                </li>
                                {userInfor == null ? (
                                    <>
                                        <li className="nav-item">
                                            <Link className="nav-link" href="/pages/login">Sign In</Link>
                                        </li>
                                        <Link href="/pages/signup">  <li className="nav-item">
                                            <button className="btn btn-success" >   Join</button>
                                        </li></Link>

                                    </>) : (
                                    <>
                                        <li>
                                            <Dropdown className='nav-item' >
                                                <Dropdown.Toggle className='nav-link position-relative ' id="dropdown-basic"
                                                    style={{
                                                        backgroundColor: 'white  ',

                                                    }}
                                                >
                                                    <img src={user?.avatar} id='img-special' style={{
                                                        width: "65px", height: "60px", borderRadius: "50%"
                                                    }} alt="" />


                                                    <div className='point position-absolute'
                                                        style={{
                                                            color: "#1dbf73", fontSize: "60px",
                                                            top: "-7px", right: "7px"
                                                        }}>.</div>
                                                </Dropdown.Toggle>
                                                <Dropdown.Menu>
                                                    <Dropdown.Item ><Link href={`/pages/profile/${userInfor?.user?.id}`} style={{
                                                        color: "black", outline: "none", textDecoration: "none"
                                                    }}>Profile</Link></Dropdown.Item>
                                                    <Dropdown.Item href="#/action-2">Settings</Dropdown.Item>
                                                    <Dropdown.Item className='text-danger' onClick={() => {
                                                        handleLogOut()
                                                    }}>
                                                        <i className="fa fa-sign-out-alt mx-1"></i>Log out</Dropdown.Item>
                                                </Dropdown.Menu>
                                            </Dropdown>
                                        </li>

                                    </>
                                )}



                            </ul>
                        </div>

                    </div>
                </div>
            </nav>
            <Menu />

        </div >
    )
}

export default Header