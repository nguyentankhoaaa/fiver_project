import { jobServ } from '@/app/services/jobServ';
import Link from 'next/link';
import React, { useEffect, useState } from 'react'

const Menu = () => {
    const [menuJob, setMenuJob] = useState([]);

    useEffect(() => {
        jobServ.getMenuCategoryJob().then((res) => {
            setMenuJob(res.data.content)
        }).catch((err) => {
            console.log(err);
        })
    }, []);
    console.log(menuJob);
    return (
        <div>
            <nav className="navjob container " >
                {
                    menuJob.map((item) => {
                        return <div className="dropdown">
                            <a className="dropbtn item">{item.tenLoaiCongViec}</a>
                            <div className="dropdown-content">
                                <ul className="p-3">
                                    <p className="text-black font-semibold" style={{ fontWeight: "bolder" }}>{item?.dsNhomChiTietLoai[0]?.tenNhom}</p>
                                    {item?.dsNhomChiTietLoai[0]?.dsChiTietLoai.map((item) => {
                                        return <Link href={`/components/header/menu/${item.id}`} className>{item.tenChiTiet}</Link>
                                    })}
                                </ul>
                            </div>
                        </div>

                    })
                }
            </nav>
        </div>
    )
}

export default Menu