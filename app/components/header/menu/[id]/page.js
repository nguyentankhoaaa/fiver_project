"use client"
import Dropdown from 'react-bootstrap/Dropdown';
import { jobServ } from '@/app/services/jobServ'
import Card from 'react-bootstrap/Card';
import React, { useEffect, useState } from 'react'
import Link from 'next/link';
const DetailMenu = ({ params }) => {
    const [chiTietLoai, setChiTietLoai] = useState([])

    useEffect(() => {
        jobServ.getJobTheoChiTIet(params.id).then((res) => {
            console.log(res.data.content);
            setChiTietLoai(res.data.content)
        })
            .catch((err) => {
                console.log(err);
            });

    }, [])

    let nameDetail = chiTietLoai[0]?.tenChiTietLoai
    console.log(chiTietLoai);
    return (
        <div className='detail-menu container ' >
            <div className='tools my-4 py-4 d-flex justify-content-between'>
                <div className='item-tool'>
                    <div className='item'>
                        <img src="https://fiverr-res.cloudinary.com/image/upload/v1584948052/general_assets/categories/nsc_01/2609.svg" alt="" />
                        <p className='mt-3 fw-semibold' >WordPress</p>
                    </div>
                </div>
                <div className='item-tool'>
                    <div className='item'>
                        <img src="https://fiverr-res.cloudinary.com/image/upload/v1584948052/general_assets/categories/nsc_01/2617.svg" alt="" />
                        <p className='mt-3 fw-semibold'>Custom Websites</p>
                    </div>
                </div>
                <div className='item-tool'>
                    <div className='item'>
                        <img src="https://fiverr-res.cloudinary.com/image/upload/v1584948052/general_assets/categories/nsc_01/2611.svg" alt="" />
                        <p className='mt-3 fw-semibold'>WIx</p>
                    </div>
                </div>
                <div className='item-tool'>
                    <div className='item'>
                        <img src="https://fiverr-res.cloudinary.com/image/upload/v1584948052/general_assets/categories/nsc_01/2613.svg" alt="" />
                        <p className='mt-3 fw-semibold'>Webflow</p>
                    </div>
                </div>
                <div className='item-tool'>
                    <div className='item'>
                        <img src="https://fiverr-res.cloudinary.com/image/upload/v1584948052/general_assets/categories/nsc_01/2612.svg" alt="" />
                        <p className='mt-3 fw-semibold'>Squarespace</p>
                    </div>
                </div>
                <div className='item-tool'>
                    <div className='item'>
                        <img src="https://fiverr-res.cloudinary.com/image/upload/v1584948052/general_assets/categories/nsc_01/2614.svg" alt="" />
                        <p className='mt-3 fw-semibold'>GoDaddy</p>
                    </div>
                </div>


            </div>
            <div className='menu-options d-flex justify-align-end'>
                <div className='col-2'>
                    <Dropdown>
                        <Dropdown.Toggle variant="light" id="dropdown-basic">
                            Dropdown Button
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                            <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                            <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                            <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
                <div className='col-2'>
                    <Dropdown>
                        <Dropdown.Toggle variant="light" id="dropdown-basic">
                            Seller details
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                            <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                            <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                            <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
                <div className='col-2'>
                    <Dropdown>
                        <Dropdown.Toggle variant="light" id="dropdown-basic">
                            Delivery time
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                            <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                            <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                            <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
            </div>
            <div className='content d-flex justify-content-between'>
                <div>
                    <h6 style={{ color: "#74767e", marginTop: "30px" }}>6,953 services available</h6>
                </div>
                <div>
                    <h6 style={{ color: "#74767e", marginTop: "30px" }}>Sort by:    <Dropdown>
                        <Dropdown.Toggle variant="light" id="dropdown-basic">
                            Delivery time
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                            <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                            <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                            <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown></h6>
                </div>
            </div>
            <h6><i className="fa fa-check-circle" style={{
                color: "#2E25AD"
            }} /> Pro Services in <span className='fw-bolder' >"{nameDetail}"</span> are offered by freelancers vetted for their skills and expertise by a dedicated Fiverr Pro team.</h6>
            <div className='row item-main my-5'>
                {chiTietLoai?.slice(0, 6).map((item) => {
                    return <div className='col-3'>
                        <Card key={item?.congViec?.id}>
                            <Link href={`/pages/jobs/${item?.congViec?.id}`}>
                                <Card.Img variant="top" src={item?.congViec?.hinhAnh} />

                            </Link>

                            <Card.Body>
                                <Card.Title className="fs-6" >{item?.congViec?.tenCongViec?.substr(1, 40)}...</Card.Title>
                                <Card.Text >
                                    <i className="fa fa-star" /> {item?.congViec?.saoCongViec}
                                    <span className="text-secondary fs-6 mx-1" >({item?.congViec?.danhGia})</span>
                                    <h6 className="my-2 fw-bolder">From US{item?.congViec?.giaTien}$</h6>
                                </Card.Text>

                            </Card.Body>
                        </Card>
                    </div>
                })}

            </div>
        </div>
    )
}

export default DetailMenu