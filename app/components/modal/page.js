"use client"
import React, { useContext, useState } from 'react'
import Modal from 'react-bootstrap/Modal';
import { message } from 'antd';
import { useForm, useWatch } from 'react-hook-form';
import { userServ } from '@/app/services/userServ';
import Context from '@/app/store/context';
const ModalShow = (props) => {
    const { register, handleSubmit, getValues, formState: { errors } } = useForm();
    const { user, setUser } = useContext(Context)

    const onsubmit = (data) => {
        const { ...newData } = data;
        userServ.putUserId(user?.id, newData).then((res) => {
            message.success("Update success !")
            setUser(res.data.content)
            console.log(res);
        })
            .catch((err) => {
                console.log(err);
                message.error("Update fails !")
            });
        console.log(newData);
    }
    return (
        <Modal className='form-update'
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter" className='fw-bolder'>
                    Update User
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className='inputs-form '>
                    <form onSubmit={handleSubmit(onsubmit)} className='form row'>
                        <div className='col-6'>
                            <form className="d-flex flex-row align-items-center">
                                <label htmlFor="email"><i className="fa fa-envelope" />
                                </label>
                                <input disabled
                                    className='input' style={{ color: "rgba(0, 0, 0, 0.38)" }} type="email" name="email"
                                    id="email" placeholder='your email' value={user?.email}
                                    {...register("email")} />
                            </form>
                            <form className="d-flex flex-row align-items-center">
                                <label htmlFor="name"><i className="fa fa-user" /></label>
                                <input className='input' type="text" name="name"
                                    id="name" placeholder='your username'
                                    {...register("name", { required: true })}
                                />

                            </form>
                            {errors.name?.type === "required" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}>Username is required</p>}

                            <form   >
                                <label htmlFor="gender" ><i className="fa fa-transgender-alt"></i></label>
                                <input className='radio' type="radio" id='male' value={true}
                                    {...register("gender", { register: true })}
                                    name='gender' />
                                <label htmlFor="male" className='radio-label'>Male</label>
                                <input className='radio' type="radio" id='female' value={false}
                                    {...register("gender", { register: true })}
                                    name='gender' />
                                <label htmlFor="male" className='radio-label'>Female</label>

                                {errors.gender?.type === "required" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}>Gender is required</p>}
                            </form>




                        </div>
                        <div className='col-6'>
                            <form className="d-flex flex-row align-items-center">
                                <label htmlFor="phone"><i className="fa fa-phone" />
                                </label>
                                <input className='input' type="phone" name="phone"

                                    {...register("phone", { required: true, pattern: /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/ })}

                                    id="phone" placeholder='your phone'

                                />
                            </form>
                            {errors.phone?.type === "required" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}> Phone-number is required</p>}
                            {errors.phone?.type === "pattern" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}>Invalid Phone-number</p>}
                            <form className="d-flex flex-row align-items-center ">
                                <label htmlFor="birthday"><i className="fa fa-birthday-cake" />
                                </label>
                                <input className='input' type="date" name="birthday"

                                    {...register("birthday", { required: true })}

                                    id="birthday" placeholder='your birthday'
                                />
                            </form>
                            {errors.birthday?.type === "required" && <p className='d-block text-danger' style={{ marginLeft: "40px", marginTop: "10px" }}> Birth-day is required</p>}
                            <form className="d-flex flex-row align-items-center">
                                <label htmlFor="skill"><i className="fa fa-book" />
                                </label>
                                <input className='input' type="text" name="skill"
                                    id="skill" placeholder='your skill'
                                    {...register("skill")} />
                            </form>

                            <button type="submit" className='item-left btn-update ' style={{ marginLeft: "2" }}>Update</button>
                        </div>
                    </form >

                </div>

            </Modal.Body>

        </Modal >

    )
}

export default ModalShow