"use client"
import { useState } from "react";
import Context from "./context";
import { localServ } from "../services/localserv";


function Provider({ children }) {
    const [userInfor, setUserInfor] = useState(localServ.get())
    const [user, setUser] = useState(localServ.getUser())
    const value = {
        userInfor, setUserInfor
    }

    return (
        <Context.Provider value={{ userInfor, setUserInfor, user, setUser }} >
            {children}
        </Context.Provider>
    )
}
export default Provider